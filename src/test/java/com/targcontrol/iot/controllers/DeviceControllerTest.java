package com.targcontrol.iot.controllers;

import com.targcontrol.iot.models.DeviceState;
import com.targcontrol.iot.views.DeviceRequest;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
//@Sql(scripts = {"classpath:clear_device_table.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:device_data.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:clear_device_table.sql"}, executionPhase = AFTER_TEST_METHOD)
class DeviceControllerTest {

  @LocalServerPort
  private int port;

  private static final String API_ROOT = "/api/v1";

  private ConrtollerTestUtil conrtollerTestUtil = new ConrtollerTestUtil();

  private static final String CREATE_NAME = "DeviceName";

  private static final Optional<UUID> ID_FOR_UPD = Optional
      .of(UUID.fromString("3a9b202d-8982-4ec0-b610-d5e1bcabafa0"));

  private static final String NAME_UPD = "deviceUPD";

  private static final UUID ID_FOR_FIND = UUID.fromString("d07c2bf1-8cb3-40fe-b61a-5f9ccc41e774");

  private static final Optional<UUID> ID_DEL = Optional
      .of(UUID.fromString("a2c816b9-7560-4066-8813-0b50bd646a21"));

  private static final String NAME_DEL = "deviceFour";

  private static final Optional<String> TOKEN_DEL = Optional
      .of("db7db901-763c-426b-8a64-343d82d1fcfd");

  public String createURLWithPort(String uri) {
    return "http://localhost:" + port + API_ROOT + uri;
  }

  @Test
  void whenSave_thenCreated() {
    DeviceRequest data = conrtollerTestUtil
        .create(Optional.empty(), CREATE_NAME, DeviceState.DEVICE_ACTIVE, Optional.empty());
    Response response = RestAssured.given()
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(data)
        .post(createURLWithPort("/device/create"));
    assertEquals(HttpStatus.CREATED.value(), response.getStatusCode());
  }

  @Test
  void whenUpdate_thenOk() {
    DeviceRequest data = conrtollerTestUtil
        .create(ID_FOR_UPD, NAME_UPD, DeviceState.DEVICE_ACTIVE, Optional.empty());
    Response response = RestAssured.given()
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(data)
        .put(createURLWithPort("/device/update"));
    assertEquals(HttpStatus.OK.value(), response.getStatusCode());
  }

  @Test
  void whenDelete_thenNoContent() {
    DeviceRequest data = conrtollerTestUtil
        .create(ID_DEL, NAME_DEL, DeviceState.DEVICE_ACTIVE, TOKEN_DEL);
    Response response = RestAssured.given()
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(data)
        .post(createURLWithPort("/device/delete"));
    assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatusCode());
  }


  @Test
  void whenGetById_thenOk() {
    Response response = RestAssured.given()
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .get(createURLWithPort("/device/" + ID_FOR_FIND));
    assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    assertNotNull(response.body());
  }

  @Test
  void whenFindAll_thenOk() {
    Response response = RestAssured.given()
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .get(createURLWithPort("/device/all"));
    assertEquals(HttpStatus.OK.value(), response.getStatusCode());
  }

}
