CREATE TABLE targcontrol_iot.sensor_data (
  id                UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
  organization_id   UUID NOT NULL,
  device_id          UUID NOT NULL,
  sensor_id          UUID NOT NULL,
  value             double precision NOT NULL,
  date_time         TIMESTAMPTZ NOT NULL
);