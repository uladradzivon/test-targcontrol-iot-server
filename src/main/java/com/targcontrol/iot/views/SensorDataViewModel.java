package com.targcontrol.iot.views;

import lombok.Data;

import java.time.ZonedDateTime;
import java.util.UUID;

@Data
public class SensorDataViewModel {

    private UUID id;

    private UUID organizationId;

    private UUID deviceId;

    private UUID sensorId;

    private ZonedDateTime dateTime;

    private Double value;
}
