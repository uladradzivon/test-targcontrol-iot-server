package com.targcontrol.iot.views;

import com.targcontrol.iot.models.DeviceState;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@Data
public class DeviceViewModel {

  private UUID id;

  @NotNull
  private String name;

  private String token;

  private Set<SensorViewModel> sensors;

  @NotNull
  private DeviceState deviceState;
}