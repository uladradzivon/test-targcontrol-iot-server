package com.targcontrol.iot.controllers;

import com.targcontrol.iot.mappers.SensorDataMapper;
import com.targcontrol.iot.models.SensorData;
import com.targcontrol.iot.services.SensorDataService;
import com.targcontrol.iot.views.SensorDataRequest;
import com.targcontrol.iot.views.SensorDataViewModel;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/sensor-data")
public class SensorDataController {

  private final SensorDataService sensorDataService;
  private final SensorDataMapper sensorDataMapper;

  @PostMapping
  public List<SensorDataViewModel> findBetweenDates(@RequestBody @Valid SensorDataRequest request) {
    List<SensorData> data = this.sensorDataService
        .findAll(request.getOrganizationId(), request.getStartDate(), request.getEndDate());
    return this.sensorDataMapper.toDto(data);
  }

}
