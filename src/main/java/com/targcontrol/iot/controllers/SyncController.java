package com.targcontrol.iot.controllers;

import com.targcontrol.iot.mappers.SensorDataMapper;
import com.targcontrol.iot.models.SensorData;
import com.targcontrol.iot.services.SensorDataService;
import com.targcontrol.iot.views.SensorDataViewModel;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/sync")
public class SyncController {

  private final SensorDataService sensorDataService;
  private final SensorDataMapper sensorDataMapper;

  @PostMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void syncSensorData(@RequestBody @Valid List<SensorDataViewModel> request) {
    List<SensorData> data = this.sensorDataMapper.fromDto(request);
    this.sensorDataService.save(data);
  }
}
