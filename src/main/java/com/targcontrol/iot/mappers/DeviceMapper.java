package com.targcontrol.iot.mappers;

import com.targcontrol.iot.models.Device;
import com.targcontrol.iot.views.DeviceViewModel;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DeviceMapper {

  DeviceViewModel toDto(Device content);

  Device fromDto(DeviceViewModel content);

  List<DeviceViewModel> toDto(List<Device> content);

  List<Device> fromDto(List<DeviceViewModel> content);

}
