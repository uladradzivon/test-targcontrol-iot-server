package com.targcontrol.iot.exceptions;

public class DuplicatedEntryException extends RuntimeException {

  private static final String ERROR_MESSAGE = "Duplicated entity: %s with value %s already exists for %s";

  public DuplicatedEntryException(String field, String value, Class<?> duplicatedEntityClass) {
    super(String.format(ERROR_MESSAGE, field, value, duplicatedEntityClass.toString()));

  }
}
